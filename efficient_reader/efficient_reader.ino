#include <TimerOne.h>
#include <SD.h>
#include <stdint.h>
#include "player.h"


const uint64_t ctc87 = 1078816946488960u; //0b1011000011111101100100101101101001010101111
const uint64_t cdb97 = 1086249320284800u;
const uint64_t dsk256 = 0u;
const uint64_t rdb55 = 0u;
const uint64_t adc237 = 0u;
const uint64_t smb377 = 0u;
const uint64_t al925 = 0u;
const uint64_t kmb364 = 0u;
const uint64_t smg26 = 0u;

#define MAX_BUF 1023
#define RAW_LEN 128
#define CYCLE_PERIOD (8u) // microseconds
#define DUTY_CYCLE_50_PERCENT (512u)
#define START_MASK (0x0F)

// states for RFID state machine
typedef enum _RfidState {
  RFID_STOP,
  RFID_WAIT,
  RFID_READ,

  NUM_RFID_STATES
} RfidState;

typedef enum _BitStoreState {
  BIT_STORE_SKIP,
  BIT_STORE_OKAY,
  BIT_STORE_END,
  BIT_STORE_ERROR
} BitStoreState;

typedef struct _RfidStatus {
  RfidState state;
  uint64_t buffer;
  uint8_t count;
  uint8_t bit;
  BitStoreState bitStatus;
  bool evenCycle;
  bool enabled;
} RfidStatus;

typedef void (*RfidRunner)(RfidStatus *const status);
typedef RfidState (*RfidSelector)(RfidStatus const *const status);
typedef void (*RfidInitializer)(RfidStatus *const status);

static void rfidExec(void);
//static void rfidRunStop(void);
static void rfidRunWait(RfidStatus *const status);
static void rfidRunRead(RfidStatus *const status);
static RfidState rfidSelectStop(RfidStatus const *const status);
static RfidState rfidSelectWait(RfidStatus const *const status);
static RfidState rfidSelectRead(RfidStatus const *const status);
static void rfidInitStop(RfidStatus *const status);
static void rfidInitWait(RfidStatus *const status);
static void rfidInitRead(RfidStatus *const status);
static BitStoreState oddBitStatus(uint8_t const bit, uint8_t const checkBit);

static RfidStatus readStatus;
static RfidRunner const RUN_STATES[NUM_RFID_STATES] =
  {NULL, rfidRunWait, rfidRunRead};
static RfidSelector const SELECT_STATES[NUM_RFID_STATES] =
  {rfidSelectStop, rfidSelectWait, rfidSelectRead};
static RfidInitializer const INIT_STATES[NUM_RFID_STATES][NUM_RFID_STATES] =
//TO: Stop          Wait          Read           //FROM:
    {{NULL,         rfidInitWait, NULL        }, //Stop
     {NULL,         NULL,         rfidInitRead}, //Wait
     {rfidInitStop, NULL,         NULL        }};//Read

static void rfidExec(void)
{
  // use global readStatus

  const RfidRunner runCurrent = RUN_STATES[readStatus.state];
  const RfidSelector selectNext = SELECT_STATES[readStatus.state];
  RfidInitializer initNext;
  RfidState nextState;

  if (!runCurrent) {
    runCurrent(&readStatus);
  }

  nextState = selectNext(&readStatus);

  initNext = INIT_STATES[readStatus.state][nextState];
  if (!initNext) {
    initNext(&readStatus);
  }
}

static void rfidRunWait(RfidStatus *const status)
{
  uint8_t const newBit = bitRead(PINB, 0);

  if (newBit) {
    status->count++;
  } else {
    status->count = 0;
  }
}

static void rfidRunRead(RfidStatus *const status)
{
  uint8_t const newBit = bitRead(PINB, 0);
  uint8_t checkBit;

  if (status->evenCycle) {
    status->bit = newBit;
    status->bitStatus = BIT_STORE_SKIP;
  } else {
    checkBit = newBit;
    status->bitStatus = oddBitStatus(status->bit, checkBit);
  }

  if (status->buffer == BIT_STORE_OKAY) {
    status->buffer &= (status->bit << status->count);
    status->count++;
  }

  // set even to odd and odd to even
  status->evenCycle = !status->evenCycle;
}

static RfidState rfidSelectStop(RfidStatus const *const status)
{
  if (status->enabled) {
    return RFID_WAIT;
  }

  return RFID_STOP;
}

static RfidState rfidSelectWait(RfidStatus const *const status)
{
  if (status->count >= 3) {
    return RFID_READ;
  }

  return RFID_WAIT;
}

static RfidState rfidSelectRead(RfidStatus const *const status)
{
  RfidState nextState;

  switch (status->bitStatus) {
    case BIT_STORE_OKAY:
    case BIT_STORE_SKIP:
      nextState = RFID_READ;
      break;
    case BIT_STORE_END:
      nextState = RFID_STOP;
    case BIT_STORE_ERROR:
      Serial.print("Bad data received.  Previous Data: ");
      //Serial.println(status->buffer, BIN);
      nextState = RFID_STOP;
    default:
      Serial.print("Bit Store Status stuck in impossible state: ");
      Serial.println(status->bitStatus, DEC);
      nextState = RFID_STOP;
  }

  return nextState;
}

static void rfidInitStop(RfidStatus *const status)
{
  status->state = RFID_STOP;
  // DO NOT RESET THE BUFFER
  status->count = 0;
  status->bit = 0;
  status->bitStatus = BIT_STORE_SKIP;
  status->evenCycle = true;
  status->enabled = false;
}

static void rfidInitWait(RfidStatus *const status)
{
  status->state = RFID_WAIT;
  status->buffer = 0;
  status->count = 0;
  status->bit = 0;
  status->bitStatus = BIT_STORE_SKIP;
  status->evenCycle = true;
  status->enabled = true;
}

static void rfidInitRead(RfidStatus *const status)
{
  status->state = RFID_READ;
  status->buffer = 0;
  status->count = 0;
  status->bit = 0;
  status->bitStatus = BIT_STORE_SKIP;
  status->evenCycle = true;
  status->enabled = true;
}

static BitStoreState oddBitStatus(uint8_t const bit, uint8_t const checkBit)
{
  if (!bit && !checkBit) {
    return BIT_STORE_END;
  } else if (bit && checkBit) {
    return BIT_STORE_ERROR;
  } else {
    return BIT_STORE_OKAY;
  }
}

void setup() {
  //audio.speakerPin = 3; //output pin for speaker
  //audio.setVolume(3);
  int SD_CS_pin = 4;
  
  Serial.begin(9600);

  if (!SD.begin(SD_CS_pin)) {
    Serial.println("SD fail");
    return;
  }

  Timer1.initialize(CYCLE_PERIOD);  // timer is set to an 8 microsecond period
  // this corresponds with 125kHz
 
  Timer1.pwm(9, DUTY_CYCLE_50_PERCENT); // Pin 9
  Timer1.attachInterrupt(rfidExec, CYCLE_PERIOD*5);
}

void loop() {
  if (readStatus.state == RFID_STOP) {
    switch (readStatus.buffer) {
      case ctc87:
        // TODO: play audio
        break;
      case cdb97:
        // TODO play audio
        break;
      default:
        Serial.println("unknown sequence collected:");
        //Serial.print(readStatus.buffer, DEC);
        //Serial.print(readStatus.buffer, DEC);
        break;
    }
  }

  // if the filename has been updated, play the
  // corresponding audio file
  if (false) {
    Timer1.stop(); // turn the timer off
    //audio.play(f_name);
    //while(audio.isPlaying()) {}
    Timer1.restart(); // turn the timer back on
    delay(2000);
  }
}

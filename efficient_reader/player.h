#ifndef _AUDIO_H
#define _AUDIO_H



typedef enum {
  PLAYER_SUCCESS,
  PLAYER_SD_CARD_NOT_FOUND,
  PLAYER_FILE_NOT_FOUND
} PlayerError;

PlayerError audioInit(void);
PlayerError playerRun(char const *const fName);

#endif // _AUDIO_H

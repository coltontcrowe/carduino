#include "player.h"
#include <SPI.h>
#include <SD.h>
#include <Audio.h>
#include <stdint.h>

PlayerError playerInit(void)
{
 uint8_t SD_CS_pin = 4;

  if (!SD.begin(SD_CS_pin)) {
    Serial.println("SD fail");
    return PLAYER_SD_CARD_NOT_FOUND;
  }

  // 44100kHz stereo => 88200 sample rate
  // 100 mSec of prebuffering.
  Audio.begin(88200, 100);

  return PLAYER_SUCCESS;
}

PlayerError playerRun(char const *const fName)
{
  int count = 0;
  File myFile = SD.open(fName);
  if (!myFile) {
    // if the file didn't open, print an error and stop
    Serial.println(fName);
    return PLAYER_FILE_NOT_FOUND;
  }

  const int S = 1024; // Number of samples to read in block
  int16_t buffer[S];

  Serial.print("Playing");
  // until the file is not finished
  while (myFile.available()) {
    myFile.read(buffer, sizeof(buffer));

    // Prepare samples
    int volume = 1024;
    Audio.prepare(buffer, S, volume);
    // Feed samples to audio
    Audio.write(buffer, S);

    // Every 100 block print a '.'
    count++;
    if (count == 100) {
      Serial.print(".");
      count = 0;
    }
  }
  myFile.close();
  return PLAYER_SUCCESS;
}
